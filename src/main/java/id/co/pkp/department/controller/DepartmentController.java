package id.co.pkp.department.controller;

import id.co.pkp.department.client.EmployeeClient;
import id.co.pkp.department.model.Department;
import id.co.pkp.department.model.Employee;
import id.co.pkp.department.repository.DepartmentRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : department
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/23
 * Time: 06:52
 * To change this template use File | Settings | File Templates.
 */
@RestController
@CrossOrigin
@Slf4j
public class DepartmentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);

    DepartmentRepository departmentRepository;
    EmployeeClient employeeClient;

    public DepartmentController(DepartmentRepository departmentRepository, EmployeeClient employeeClient) {
        this.departmentRepository = departmentRepository;
        this.employeeClient = employeeClient;
    }

    @PostMapping("/")
    public Department add(@RequestBody Department department) {
        LOGGER.info("Department add: {}", department);
        return departmentRepository.add(department);
    }

    @GetMapping("/{id}")
    public Department findById(@PathVariable("id") Long id) {
        LOGGER.info("Department find: id={}", id);
        return departmentRepository.findById(id);
    }

    @GetMapping("/")
    public List<Department> findAll() {
        LOGGER.info("Department find");
        return departmentRepository.findAll();
    }

    @GetMapping("/organization/{organizationId}")
    public List<Department> findByOrganization(@PathVariable("organizationId") Long organizationId) {
        LOGGER.info("Department find: organizationId={}", organizationId);
        return departmentRepository.findByOrganization(organizationId);
    }

    @GetMapping("/organization/{organizationId}/with-employees")
    @CircuitBreaker(name = "getOrganizationWithEmployees", fallbackMethod = "getOrganizationWithEmployeesFallback")
    public List<Department> findByOrganizationWithEmployees(@PathVariable("organizationId") Long organizationId) {
        LOGGER.info("Department find: organizationId={}", organizationId);
        List<Department> departments = departmentRepository.findByOrganization(organizationId);
        departments.forEach(d -> d.setEmployees(employeeClient.findByDepartment(d.getId())));
        return departments;
    }

    public List<Department> getOrganizationWithEmployeesFallback(Long organizationId, Exception e) {
        List<Department> departmentList = new ArrayList<>();
        log.info("---RESPONSE FROM FALLBACK METHOD---");
//        return "SERVICE IS DOWN, PLEASE TRY AFTER SOMETIME !!!";
        Employee employee = new Employee(
                "SERVICE IS DOWN, PLEASE TRY AFTER SOMETIME !!!",
                0,
                "SERVICE IS DOWN, PLEASE TRY AFTER SOMETIME !!!");

        Department department = new Department();
        department.setId(0L);
        department.setName("SERVICE IS DOWN, PLEASE TRY AFTER SOMETIME !!!");
        department.setOrganizationId(0L);
        department.setEmployees(List.of(employee));
        departmentList.add(department);
        return departmentList;
    }
}
