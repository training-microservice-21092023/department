package id.co.pkp.department.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : department
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/21/23
 * Time: 06:50
 * To change this template use File | Settings | File Templates.
 */
@Data
@NoArgsConstructor
public class Employee {
    private Long id;
    private String name;
    private int age;
    private String position;

    public Employee(String name, int age, String position) {
        this.name = name;
        this.age = age;
        this.position = position;
    }
}
